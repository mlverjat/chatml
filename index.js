// -1-FAIRE APPEL A EXPRESS qui sera gestionnaire de fonctions ()
// Avec la variable app, je charge avec require le framework Node js express
var app = require('express')();
// -2-CHARGER HTTP 
var http = require('http').Server(app);
// -3- INITIALISER UNE INSTANCE DE SOCKET IO en passant l'objet http(le serveur HTTP). CHARGER SOCKET IO
var io = require('socket.io')(http);
//-4- DEFINITION DE ROUTE SIMPLE, à chaque fois que quelqun visite notre page index.html
//app est une instance d'express, get est une méthode de demande HTTP [https://expressjs.com/fr/starter/basic-routing.html]
// pour la methode get, cette ligne définie la route jusqu'à l'application.
//Les REUETES HTTP en 3 mots:
//req = message de requête HTTP au serveur. Le serveur, qui fournit des ressources telles que des fichiers HTML et d'autres contenus,
// ou exécute d'autres fonctions pour le compte du client, renvoie un message de réponse au client res.send (contenurenvoyé). La réponse contient des 
//informations sur l'état d'achèvement de la demande et peut également contenir le contenu demandé dans le corps du message.
app.get('/', function(req, res){
  res.sendfile('index.html');//envoyer pour la réponse le fichier index.html 
});
//A chaque fois que la var io , appliquer la méthode on,écouter l'évènement connection lorsque les sockets  et je le connecte à la console.
io.on('connection', function(socket){
  socket.on('chat message', function(msg){ //associer à socket lorsqu'il est déclenché, un évènement chatmessage avec une fonction avec paramètre message
    io.emit('chat message', msg);//fonction qui envoit un message à l'utilisateur
  });
});

http.listen(3000, function(){
  console.log('listening on *:3000'); // Quand le navigateur se connecte à 3000, afficher.
});

// En parallèle, installer les modules express https://expressjs.com/fr/starter/installing.html et socket

//aller dans son dossier ou on a ses fichiers 
//faire npm init (mis modules npm ds mon fichier Génère un fichier package.json qui décrit la config du projet )
//puis install express npm install express --save......
//[https://expressjs.com/fr/starter/installing.html]
// Après écrire dans le terminal node index.js pour visualiser ce fichier !! ENFIN!! ca lance la page , écoute le port 3000 sur mon serveur local donc je dois l'appeler
//PFF GRR ... Le terminal listening on *:3000
// à ce moment là on va sur le navigateur on copie colle http://localhost:3000 dans le navigateur 
// OUVERTURE SUR PAGE HTML néant, ca s'execute pas dessus ca n'affiche pas le contenu ds page html
//ca l'execute 
//

